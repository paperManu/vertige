Vertige
=======

'Vertige' is a demonstration of the use of Metalab's tools to hybridize the real with the virtual, through an individual experience adapting the visual perspective of one person to their movements.

## Description

The tools involved are:
* [Splash](https://gitlab.com/sat-metalab/splash) for the video mapping
* [LivePose](https://gitlab.com/sat-metalab/livepose) for the head tracking, through pose estimation
* [Blender](https://blender.org) to help with recomputing the UV coordinates in real time
* [shmdata](https://gitlab.com/sat-metalab/shmdata) for sending mesh buffers between Blender and Splash
* some Python scripts

The directories are organized as follows:
* assets: some assets used in the project, in particular the scene from [Mantissa](mantissa.xyz) which has been modified for immersive rendering
* blender: Blender files and scripts, for testing UV recomputing
* livepose: LivePose configuration
* splash: Splash configuration, scripts and assets
* study: a Blender test files to evaluate the projector placement for the piece

## Running

Before anything else, Splash must be calibrated for the immersive space. Please refer to its [documentation](https://sat-metalab.gitlab.io/splash) to gather information regarding how to do that.

LivePose must also be calibrated. As this demonstration is very bare-metal, the calibration values are hard coded in two Python scripts:
* the script embedded in the Blender file `splash/immersive_space.blend`, lines 157 to 185
* the Splash script in `splash/scripts/osc_server.py`, lines 157 to 182

Ideally the values should be identical in both scripts. In a perfect world both of these scripts would be merged into a single one. The calibration is meant to get values from LivePose in the same space coordinates as the Splash scene.

Now to get to the running part. All of the tools communicate together but they can be ran in whichever order, which is a good news. Let's start with LivePose:

```bash
cd /path/to/livepose/directory
source /path/to/livepose/venv
./liveposeh.sh -c /path/to/this/directory/livepose/config.json
```

This will run LivePose with the configuration from `livepose/config.json`, and send OSC messages to `localhost` on both ports 9000 and 9010. Splash will be listening on port 9000, and Blender on port 9010.

Now we get to run Blender:

```bash
blender splash/immersive_space.blend
```

Upon running, Blender might as you whether you allow for running scripts from this file automatically. You can answer yes to that, otherwise you will have to run the script manually every time to open this file.

Once open, move the mouse cursor over the 3D viewport in the lower left corner, then press `ctrl + space`. This will maximize this view.

Then press `space` to run the timeline. This will make Blender update the texture coordinates of the displayed object according to the detected head position in the physical space by LivePose.

Lastly, run Splash:

```bash
splash splash/config.json
```

This will run Splash. The default configuration considers a `1920x1080` display and a separate `3840x2160` videoproejctor. As said earlier you should have updated the calibration beforehand.

Splash should now receive updated textured mesh from Blender, and OSC messages from LivePose to adjust the 3D view to the detected head position.

And that's when the 'Vertige' should happen!
