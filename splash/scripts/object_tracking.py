#!/usr/bin/env python3
# This script controls a Splash object from a VRPN tracker
import argparse
import code
import sys
import threading
import time

import splash  # type: ignore

from osc_server import OSCServer

repl_active = False
hostname = 'localhost'
oscserver = None
run_loop = True
eye_position = [0.0, 0.0, 0.0]
last_updated = time.time()


try:
    import readline
except ImportError:
    pass
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
    readline.set_completer(rlcompleter.Completer(globals()).complete)


def repl():
    global console
    console = code.InteractiveConsole(locals=globals())
    console.interact()


replThread = threading.Thread(target=repl)


def parse_arguments():
    global hostname, tracker_links, repl_active
    if not len(sys.argv):
        return

    parser = argparse.ArgumentParser()
    parser.add_argument("--repl", help="active the REPL", action='store_true')
    args = parser.parse_args()

    repl_active = args.repl


def splash_init():
    global oscserver
    parse_arguments()
    try:
        oscserver = OSCServer('localhost', 9010)
        print("OSC server running at: ", oscserver.getPort())
        oscserver.recv(0)
    except Exception as e:
        oscserver = None
        print("could not start OSC server - ", e)

    if repl_active:
        replThread.start()


def splash_loop():
    global eye_position
    global last_updated

    oscserver.recv(0)
    new_eye_position = oscserver.get_closest_follower(max_distance=3.0)

    if new_eye_position is None:
        current_time = time.time()
        if current_time - last_updated > 3.0:  # No update for more than 3 seconds
            new_eye_position = [-1.0, -1.0, 1.7]
            
    if new_eye_position is None:
        return True
    
    last_updated = time.time()

    if new_eye_position != eye_position:
        eye_position = new_eye_position
        splash.set_object_attribute("virtual_probe_1", "position", eye_position)

        # splash.set_object_attribute("texCoordGenerator", "eyeOrientation", [0.0, 1.0, 0.0])
        # splash.set_object_attribute("texCoordGenerator", "method", 'Equirectangular projection')
        # splash.set_object_attribute("texCoordGenerator", "eyePosition", eye_position)
        # splash.set_object_attribute("texCoordGenerator", "meshName", "mesh")
        # splash.set_object_attribute("texCoordGenerator", "generate", True)
        # splash.set_object_attribute("texCoordGenerator", "meshName", "mesh_8")
        # splash.set_object_attribute("texCoordGenerator", "generate", True)

    # Make sure that the window is at the correct position at any time
    splash.set_object_attribute("window_1", "position", [1920, 0])

    return True


def splash_stop():
    global run_loop
    run_loop = False
    if repl_active:
        print("Press a key to quit")
        console.push("quit()")
