# import bpy
import liblo
import math
import numpy as np
import re
import time

from typing import Dict, List, Optional


# One Euro Filter, as described in:
# https://hal.inria.fr/hal-00670496/document
# Source code taken from https://cristal.univ-lille.fr/~casiez/1euro/OneEuroFilter.py
class LowPassFilter(object):

    def __init__(self, alpha):
        self.__setAlpha(alpha)
        self.__y = self.__s = None

    def __setAlpha(self, alpha):
        alpha = float(alpha)
        if alpha<=0 or alpha>1.0:
            raise ValueError("alpha (%s) should be in (0.0, 1.0]" % alpha)
        self.__alpha = alpha

    def __call__(self, value, timestamp=None, alpha=None):
        if alpha is not None:
            self.__setAlpha(alpha)
        if self.__y is None:
            s = value
        else:
            s = self.__alpha * value + (1.0 - self.__alpha) * self.__s
        self.__y = value
        self.__s = s
        return s

    def lastValue(self):
        return self.__y


class OneEuroFilter(object):

    def __init__(self, freq, mincutoff=1.0, beta=0.0, dcutoff=1.0):
        if freq<=0:
            raise ValueError("freq should be >0")
        if mincutoff<=0:
            raise ValueError("mincutoff should be >0")
        if dcutoff<=0:
            raise ValueError("dcutoff should be >0")
        self.__freq = float(freq)
        self.__mincutoff = float(mincutoff)
        self.__beta = float(beta)
        self.__dcutoff = float(dcutoff)
        self.__x = LowPassFilter(self.__alpha(self.__mincutoff))
        self.__dx = LowPassFilter(self.__alpha(self.__dcutoff))
        self.__lasttime = None

    def __alpha(self, cutoff):
        te = 1.0 / self.__freq
        tau = 1.0 / (2 * math.pi * cutoff)
        return 1.0 / (1.0 + tau / te)

    def __call__(self, x, timestamp=None):
        # ---- update the sampling frequency based on timestamps
        if self.__lasttime and timestamp:
            self.__freq = 1.0 / (timestamp - self.__lasttime)
        self.__lasttime = timestamp
        # ---- estimate the current variation per second
        prev_x = self.__x.lastValue()
        dx = 0.0 if prev_x is None else (x - prev_x) * self.__freq  # FIXME: 0.0 or value?
        edx = self.__dx(dx, timestamp, alpha=self.__alpha(self.__dcutoff))
        # ---- use it to update the cutoff frequency
        cutoff = self.__mincutoff + self.__beta * math.fabs(edx)
        # ---- filter the given value
        return self.__x(x, timestamp, alpha=self.__alpha(cutoff))


class OSCServer():
    def __init__(self, host, port=9000):
        self.port = port
        self.server = liblo.Server(port)
        self.path_specs = []
        self.registerCallbacks()
        self.debug = False
        self.host = host

        self.follower_positions: Dict[List[float]] = {}

        self.position_filter_config = {
            'freq': 120,  # Hz
            'mincutoff': 1.0,
            'beta': 1.0,
            'dcutoff': 1.0  # Do not modify?
        }
        
        self.position_filters: List[OneEuroFilter] = []
        for index in range(3):
            self.position_filters.append(OneEuroFilter(**self.position_filter_config))

    def stop(self):
        self.server.free()
        del self.server

    def getPort(self):
        return self.port

    def recv(self, timeout):
        try:
            while(self.server.recv(timeout)):
                pass
        except Exception as e:
            print("cannot receive because ", e)

    def registerCallbacks(self):
        self.server.add_method(None, None, self.dispatch_message)
        self.add_regex_method("/livepose/pyrealsense_eyes_follower/0/(?P<n>.+)", "s", self.handle_object)
        self.add_regex_method("/livepose/pyrealsense_eyes_follower/start_filter", "s", self.reset_followers)

    def add_regex_method(self, spec, type, func):
        self.path_specs.append((spec, type, func))

    def dispatch_message(self, path, args, types, src):
        if self.debug:
            print("received OSC message ", path, args, types, src)
        matched = False
        for spec, t, f in self.path_specs:
            matches = re.match(spec, path)
            if matches is not None:
                d = matches.group()
                if self.debug:
                    print("matched group ", d)
                f(path, args)
                matched = True
        if not matched:
            if self.debug:
                print("not matched: ", path)
            pass

    def reset_followers(self, path, args):
        if self.debug:
            print("message for {} with following arguments: {}".format(path, args))
            
        self.follower_positions.clear()

    def handle_object(self, path, args):
        if self.debug:
            print("message for {} with following arguments: {}".format(path, args))

        if len(args) != 3:
            return

        if args[0] == 0.0 and args[1] == 0.0 and args[2] == 0.0:
            return

        follower_id = int(self.handle_path(path))

        angle = -math.pi / 4.0
        rot_mat = np.matrix([
            [1.0, 0.0, 0.0],
            [0.0, math.cos(angle), -math.sin(angle)],
            [0.0, math.sin(angle), math.cos(angle)]
        ])
        eye = np.array([args[0], -args[1], args[2]])
        eye_rotated = np.array(rot_mat.dot(eye).tolist()[0])

#        angle = -math.pi / 2.0
#        rot_mat = np.matrix([
#            [math.cos(angle), -math.sin(angle), 0.0],
#            [math.sin(angle), math.cos(angle), 0.0],
#            [0.0, 0.0, 1.0]
#        ])
#        eye_rotated = np.array(rot_mat.dot(eye_rotated).tolist()[0])
        eye_position = eye_rotated - np.array([-0.9, 0.0, 2.40])

        if self.debug:
            print(f"Unfiltered position: {eye_position}")

        current_time = time.time()
        for index in range(3):
            eye_position[index] = self.position_filters[index](eye_position[index], current_time)

        self.follower_positions[follower_id] = [-eye_position[0], -eye_position[1], -eye_position[2]]

    def get_closest_follower(self, max_distance: float = 0.0) -> Optional[List[float]]:
        if len(self.follower_positions) == 0:
            return None
        
        # Get the closest follower
        closest_id = None
        closest_dist = 65536.0  # 65536 meters for the base closest should be more that enough
        for index, position in self.follower_positions.items():
            distance = math.sqrt(position[0] * position[0] + position[1] * position[1])
            if distance < closest_dist:
                closest_dist = distance
                closest_id = index
        
        if max_distance != 0 and closest_dist > max_distance:
            return None
                
        if closest_id is not None:
            return self.follower_positions[closest_id]

    def handle_path(self, p):
        """split the path and return the last element"""
        return p.split("/")[-1]


# osc_server = OSCServer("localhost", 9000)
# eye_position = [0.0, 0.0, 0.0]
# last_updated = time.time()
# 
# 
# def get_osc(scene: bpy.types.Scene, depsgraph: bpy.types.Depsgraph) -> None:
#     global eye_position
# 
#     osc_server.recv(0)
#     new_eye_position = osc_server.get_closest_follower(max_distance=3.0)
#     
#     if new_eye_position is None:
#         current_time = time.time()
#         if current_time - last_updated > 3.0:  # No update for more than 3 seconds
#             new_eye_position = [1.0, 2.0, 1.7]
#             
#     if new_eye_position is None:
#         return
#     
#     last_updated = time.time()
#     
#     if eye_position != new_eye_position:
#         eye_position = new_eye_position
#         bpy.data.scenes["Scene"].cursor.location = eye_position
#         bpy.ops.uv.sphere_project()
# 
# 
# if get_osc not in bpy.app.handlers.frame_change_post:
#     bpy.app.handlers.frame_change_post .append(get_osc)
